# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=modemmanager-qt
pkgver=5.100.0
pkgrel=0
pkgdesc="Qt wrapper for ModemManager DBus API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	modemmanager-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # requires dbus running
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4d02b3fcf48e8e1503e8150610562247849b8cb62bc6d579ec687033fb06a4e0cefa611797472e35979d8d513f7ae2f095701d9f40e8bbcaef2cc96f017ab1e  modemmanager-qt-5.100.0.tar.xz
"
