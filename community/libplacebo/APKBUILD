# Contributor: Alex Yam <alex@alexyam.com>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Alex Yam <alex@alexyam.com>
pkgname=libplacebo
pkgver=5.229.1
pkgrel=0
pkgdesc="Reusable library for GPU-accelerated video/image rendering"
url="https://code.videolan.org/videolan/libplacebo"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	glfw-dev
	glslang-dev
	glslang-static
	lcms2-dev
	libepoxy-dev
	meson
	py3-glad
	py3-mako
	sdl2-dev
	sdl2_image-dev
	shaderc-dev
	spirv-tools-dev
	vulkan-loader-dev
	"
subpackages="$pkgname-dev"
source="https://code.videolan.org/videolan/libplacebo/-/archive/v$pkgver/libplacebo-v$pkgver.tar.gz"
builddir="$srcdir/libplacebo-v$pkgver"

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Ddemos=false \
		-Dglslang=enabled \
		-Dlcms=enabled \
		-Dshaderc=enabled \
		-Dtests=true \
		-Dvulkan=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
45efb252d5d44e5429790f9024b1b2a153051d6d604b07fcf3e4de7d49a2e91d20457be76e08b3b9ab43248c4a2e7268c9f8dc69837783e572f9b8735c955334  libplacebo-v5.229.1.tar.gz
"
