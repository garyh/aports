# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtsvg
pkgver=6.4.1
pkgrel=0
pkgdesc="Classes for displaying the contents of SVG files"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtsvg-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtsvg-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fa9d5f13c1aefc31d29303b2e32ef3a35e9a50ab98a5e263a2a12bf645d63546daf5d131a892ed8648aa231d3a837a5319f00e82800ca4bee7344a4d94e6305f  qtsvg-everywhere-src-6.4.1.tar.xz
"
