# Contributor: Milan P. Stanić <mps@arvanta.net>
# Maintainer: Milan P. Stanić <mps@arvanta.net>
pkgname=eiwd
_realpkgname=iwd
pkgver=2.0
pkgrel=0
pkgdesc="Internet Wireless Daemon without dbus"
url="https://github.com/illiliti/eiwd"
arch="all"
license="LGPL-2.1-or-later"
depends="!iwd"
replaces="iwd"
options="!check" # doesn't work because of eapol issue
makedepends="linux-headers autoconf automake libtool coreutils"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://github.com/illiliti/eiwd/releases/download/$pkgver-1/iwd-$pkgver.tar.xz
	eiwd.initd
	main.conf
	eiwd.post-upgrade
	iwd_passphrase
	"
builddir="$srcdir/iwd-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-systemd-service \
		--disable-dbus
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -m750 -d "$pkgdir"/var/lib/$_realpkgname
	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/main.conf \
		"$pkgdir"/etc/$_realpkgname/main.conf
	install -m755 -D "$srcdir"/iwd_passphrase \
		"$pkgdir"/usr/bin
}

sha512sums="
28b0b65ca179bf52bf91ff2a3414aee54dbe34e84f137e37a1c1196cb82bad26b6bd51d8a95abeeebacb27048dc52d28ddc0cde2cc18d63a755a43ca9d2caa37  iwd-2.0.tar.xz
cf4eca6baaf75cf345a4f8f13d27da65780ad67b64aa42b89b45199279b1ef794af0d28df952530bb7816ff2903801973f0592e8edc42ec221e6b49c7738edec  eiwd.initd
c126da929709ec5a52df0c10d2e428b14b5ce137e3919692920a1e309ac9e12ecd8c75529066c72fee5b7ee2cf26ec2574d0447e122f52a81f3d3ae7077e87e6  main.conf
414cb08e01735a66dfb57eac7f308ce75f8f7adf679c5e38418293a0acb2398f9b4df4dd50a6756e48eb03b86de1546e2f852a28677842f2c453a0db86d49f71  eiwd.post-upgrade
06d0258e9e4d13a5cd7cb47cc30c122e2ea950c84b18352fef4982bef41961295316de396b7e6d0a33b949436da40d08fdd76230c7c228d8bdd0e33751cad26c  iwd_passphrase
"
